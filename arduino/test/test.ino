int currentLine = 0;
void setup()
{
  Serial.begin(115200);
}

void loop()
{
  const int fakeDataLength = 10;
  if (currentLine >= fakeDataLength)
  {
    currentLine = 0;
    //currentLine = random(0, fakeDataLength);
  }
    
  switch(currentLine)
  {
    case 0:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 4,\"SensorData\": [2193,2181,2170,2207,2193,2183,2176,2175,2199,2196,2186,2170,2209,2197,2185,2180,2175,2201,2195,2183]}");
    break;
    case 1:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 1,\"SensorData\": [2250,2238,2278,2275,2260,2251,2281,2272,2264,2252,2282,2272,2264,2252,2282,2273,2264,2253,2282,2274,2266]}");
    break;
    case 2:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 5,\"SensorData\": [2281,2175,2211,2208,2197,2186,2214,2207,2197,2187,2216,2225,2215,2187,2214,2225,2198,2187,2214,2206,2198,2188]}");
    break;
    case 3:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 2,\"SensorData\": [2225,2282,2281,2268,2279,2291,2281,2269,2280,2292,2282,2270,2281,2290,2282,2268,2280,2291,2280,2267,2251,2291]}");
    break;
    case 4:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 6,\"SensorData\": [2270,2211,2213,2234,2217,2208,2219,2230,2220,2209,2220,2231,2215,2207,2220,2229,2220,2208,2220,2231,2221,2209]}");
    break;
    case 5:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 3,\"SensorData\": [2232,2295,2265,2281,2293,2281,2271,2283,2293,2282,2270,2283,2292,2282,2270,2283,2292,2284,2272,2287,2295,2284]}");
    break;
    case 6:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"SensorId\": 1,\"TestStageId\": 7,\"SensorData\": [2287,2217,2240,2227,2214,2231,2236,2227,2215,2229,2237,2227,2216,2231,2237,2228,2217,2231,2238,2228,2216,2230]}");
    break;
    case 7:
      Serial.println("{\"MacAddress\": \"F0:B5:D1:88:11:EA\",\"FirmwareVersion\": \"URGO_\",\"HardwareVersion\": \"4_2_2\",\"Battery\": 94\r,\"Min\": 0,\"Max\": 0,\"SensorId\": 1,\"TestStageId\": 8,\"SensorData\": [2287,2217,2240,2227,2214,2231,2236,2227,2215,2229,2237,2227,2216,2231,2237,2228,2217,2231,2238,2228,2216,2230]}");
    break;
    case 8:
      Serial.println("94");
    break;
    case 9:
      Serial.println("100");
    break;
  }
  currentLine++;
  delay(1000);
  //Serial.flush();
}

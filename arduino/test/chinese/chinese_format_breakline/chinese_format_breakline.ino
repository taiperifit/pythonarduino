int currentLine = 0;
int currentTest = 0;
void setup()
{
  Serial.begin(115200);
}

void loop()
{
  const int fakeDataLength = 5;
  const int fakeTestLength = 100;

  if (currentLine >= fakeDataLength)
  {
    currentLine = 0;
    currentTest++;
    //currentLine = random(0, fakeDataLength);
  }
  if (currentTest >= fakeTestLength)
  {
    currentTest = 0;
    //currentLine = random(0, fakeDataLength);
  }
    
  switch(currentLine)
  {
    case 0:
      Serial.println("{\"MacAddress\": \"F0:D1:7E:F4:0A\",\"FirmwareVersion\": \"Perif\",\"HardwareVersion\": \"4_2_2\",\"Battery\": 91");
    break;
    case 1:
      Serial.println(",\"Min\": 0,\"Max\": 0,\"SensorId\": 1,\"TestStageId\": 7,\"SensorData\": [1621,1642,1644,1639,1623,1648,1651,1640,1623,1647,1647,1646,1631,1653,1647,1511,-1,1604,1632,1634,1632,1616,1633,1633,1630,1619,1636,1636,1630,1619,1636]}");
    break;
    case 2:
      Serial.println("{\"MacAddress\": \"F0:D1:7E:F4:0A\",\"FirmwareVersion\": \"Perif\",\"HardwareVersion\": \"4_2_2\",\"Battery\": 91");
    break;
    case 3:
      Serial.println(",\"Min\": 0,\"Max\": 0,\"SensorId\": 1,\"TestStageId\": 8,\"SensorData\": [1710,1683,1682,1677,1659,1685,1684,1677,1670,1685,1684,1678,1661,1684,1683,1678,-1,1734,1673,1658,1682,1680,1675,1659,1682,1681,1675,1658,1682,1681,1674,1659,1679]}");
    break;
    case 4:
      Serial.println("70");
    break;
  }
  currentLine++;
  
  delay(1000);
  //Serial.flush();
}

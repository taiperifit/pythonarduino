int currentLine = 0;
int currentTest=0;
void setup()
{
  Serial.begin(115200);
}

void loop()
{
  const int fakeDataLength = 2;
  if (currentLine >= fakeDataLength)
  {
    currentLine = 0;
    currentTest++;
    //currentLine = random(0, fakeDataLength);
  }
  const int fakeTestLength = 100;
  if (currentTest >= fakeTestLength)
  {
    currentTest = 0;
    //currentLine = random(0, fakeDataLength);
  }
    
  switch(currentLine)
  {
    case 0:
      Serial.println("{\"MacAddress\": \"F0:"+String(currentTest,HEX)+":D1:7E:F4:0A\",\"FirmwareVersion\": \"Perif\",\"HardwareVersion\": \"4_2_2\",\"Battery\": 100,\"Min\": 0,\"Max\": 0,\"SensorId\": 1,\"TestStageId\": 8,\"SensorData\": [1728,1707,1732,1731,1726,1719,1732,1731,1725,1707,1732,1731,1724,1705,1731,1731,-1,1668,1730,1722,1706,1729,1729,1723,1705,1730,1729,1723,1706,1730,1729,1723,1706]}");
    break;
    case 1:
      Serial.println("{\"MacAddress\": \"F0:"+String(currentTest,HEX)+":D1:7E:F4:0A\",\"FirmwareVersion\": \"Perif\",\"HardwareVersion\": \"4_2_2\",\"Battery\": 100,\"Min\": 0,\"Max\": 0,\"SensorId\": 1,\"TestStageId\": 7,\"SensorData\": [1710,1683,1682,1677,1659,1685,1684,1677,1670,1685,1684,1678,1661,1684,1683,1678,-1,1734,1673,1658,1682,1680,1675,1659,1682,1681,1675,1658,1682,1681,1674,1659,1679]}");
    break;
    case 3:
      Serial.println("99");
    break;
    case 4:
      Serial.println("70");
    break;
  }
  currentLine++;
  
  delay(1000);
  //Serial.flush();
}

A. Requirements
    https://www.notion.so/Python-script-specs-6e14f1242ca74340a15533069cd6b978

B. How to run
1. Install python 3x: https://realpython.com/installing-python/
2. Install pyserial: pip3 install pyserial
3. Install termcolor: pip3 install termcolor
4. Open terminal (on MacOS) or Command Line (on Windows) and cd into folder python/
5. Run command: python3 run.py
6. Enter serial port from Arduino (the port you are using to upload arduino code at bottom right of Arduino IDE)
7. Next time, the python app will auto open the last port, if the port is not available, the app will ask for enter serial port name

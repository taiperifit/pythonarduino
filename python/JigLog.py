# -*- coding: utf-8 -*-
"""Module importation"""
import serial
import json
import os
import datetime
import Const
from termcolor import colored, cprint # print something with color

from threading import Thread
from ExcelRow import ExcelRow
from HttpRequestHandler import HttpRequestHandler
from ArduinoPortManager import ArduinoPortManager

class JigLog(Thread):
    httpRequestHandler = None
    arduinoPortManager = None
    def __init__(self, isArduinoDevice):
        super(JigLog, self).__init__()

        self.exportFolder = Const.EXPORT_FOLDER
        self.exportHeaderFormat = "MacAddress,Session date,Session time,Sensor Id,TestStageId 4,%s,TestStageId 1,%s,TestStageId 5,%s,TestStageId 2,%s,TestStageId 6,%s,TestStageId 3,%s,TestStageId 7,%s,FirmwareVersion,HardwareVersion,Battery,Status"
        self.excelFileNameFormat = self.exportFolder + "/Slot%d_TestJigLog_%s.csv" # example: TestJigLog_yyyy-mm-dd
        self.excelFileIO = None
        self.excelRow = ExcelRow()
        self.slot = 0 #default is 0 if we run with only 1 jig
        self.arduino = None
        self.isValidPort = False
        self.isArduinoDevice = isArduinoDevice
        self.packageFailCount = 0
        self.portName = ''

    def setSlot(self, slot) :
        self.slot = slot

    def computeSensorTimeHeader(self, startIndex, maxColumns):
        sensorTimeHeader = ""
        for x in range(startIndex, startIndex + maxColumns - 1):
            sensorTimeHeader = sensorTimeHeader + "Time %d," % (x + 1)
        sensorTimeHeader = sensorTimeHeader + ("Time %d" % (startIndex + maxColumns))
        return sensorTimeHeader

    def createFileHeader(self):
        sensorTimeHaders = []
        for x in range(7):
            sensorTimeHaders.append(self.computeSensorTimeHeader(x * Const.MAX_SENSOR_VALUE_COUNT, Const.MAX_SENSOR_VALUE_COUNT))
        return self.exportHeaderFormat % (  sensorTimeHaders[0], 
                                            sensorTimeHaders[1],
                                            sensorTimeHaders[2],
                                            sensorTimeHaders[3],
                                            sensorTimeHaders[4],
                                            sensorTimeHaders[5],
                                            sensorTimeHaders[6] )

    def openFileAndWriteHeader(self) :
        fileName = self.excelFileNameFormat % (self.slot, datetime.datetime.now().date())
        # print("Open file: " + fileName)
        # create Exported folder if it doesn't exist
        if os.path.isdir(self.exportFolder) == False :
            os.mkdir(self.exportFolder)

        if os.path.isfile(fileName):
            self.excelFileIO = open(fileName, "a+")
        else:
            self.excelFileIO = open(fileName, "a+")
            self.excelFileIO.write(self.createFileHeader() + '\n')

            # save file
            self.excelFileIO.close()

            # open file for save data
            self.excelFileIO = open(fileName, "a+")

    
    def readPortName(self):
        # create Config folder if it doesn't exist
        configFolder = Const.CONFIG_FOLDER
        configFileName = "%s/last_port_name%d.config" %(configFolder, self.slot)
        if os.path.isdir(configFolder) == False :
            os.mkdir(configFolder)
            
        if os.path.isfile(configFileName) == False:
            return ""
        
        file=open(configFileName, mode='r')
        portName = file.readline()
        file.close()
        return portName

    def savePortName(self, portName):
        configFolder = Const.CONFIG_FOLDER
        configFileName = "%s/last_port_name%d.config" %(configFolder, self.slot)
        file=open(configFileName, mode='w')
        file.write(portName)
        file.close()

    def dumpDataToFile(self):
        # send data to backend
        self.httpRequestHandler.addData(self.excelRow.getPeakBackendData())
        self.httpRequestHandler.addData(self.excelRow.getPlateauxBackendData())

        # print(self.httpRequestHandler.dataQueue)

        # dump data to file
        self.excelRow.dumpDataToFile(self.excelFileIO)

        # reset data for next session
        self.excelRow.reset()

        # save file
        cprint("[Port: %s] Session complete and saved data" % self.portName, "white")
        self.excelFileIO.close()

        # open file again for next row
        self.openFileAndWriteHeader()
        
        # create new row
        # cprint("Waiting for data from next session", "white")

    #lineInput is line received from Arduino
    def processLine(self, lineInput): 
        try:
            # make sure lineInput is a json string
            lineInput = lineInput.strip()
            if lineInput.startswith("{") == False :
                return
            if lineInput.endswith("}") == False :
                return

            line = json.loads(lineInput)
            
            # print("Read json data: " + lineInput)
            try:
                macAddress = line["MacAddress"]
                sensorId = line["SensorId"]
                testStageId = line["TestStageId"]
                sensorData = line["SensorData"]

                # contain -1 value is Chinese format
                isChineseFormat = -1 in sensorData
                self.excelRow.setStageFormat(isChineseFormat)

                if isChineseFormat :
                    if testStageId == Const.CHINESE_FORMAT_START_STATE: # new session
                        # cprint("Chinese format", "yellow")
                        #complete at least one session
                        if self.isArduinoDevice == False:
                            self.isArduinoDevice = True
                            cprint("Save port: %s" % self.portName, "green")
                            self.arduinoPortManager.addValidPortName(self.portName)
                            self.arduinoPortManager.saveValidPortName()

                        # save last session if any
                        if self.excelRow.macAddress != "":
                            self.dumpDataToFile()

                        # update data for new state
                        self.excelRow.macAddress = macAddress
                        self.excelRow.sensorId = sensorId
                        self.excelRow.firmwareVersion = line["FirmwareVersion"]
                        self.excelRow.hardwareVersion = line["HardwareVersion"]
                        self.excelRow.battery = line["Battery"]

                        # fetch sensor values for peeks
                        startPeekIndex = 0
                        for x in sensorData:
                            if x < 0 :
                                startPeekIndex += 1
                            else:
                                self.excelRow.sensorData[startPeekIndex].append(x)

                        # print(self.excelRow.sensorData[0])
                        # print(self.excelRow.sensorData[1])

                    elif self.excelRow.macAddress != macAddress and self.excelRow.macAddress != "":
                        self.dumpDataToFile()
                    elif self.excelRow.sensorId != sensorId and self.excelRow.sensorId != 0:
                        self.dumpDataToFile()
                    elif testStageId == Const.CHINESE_FORMAT_END_STATE: # complete the test
                        self.excelRow.macAddress = macAddress
                        self.excelRow.sensorId = sensorId
                        self.excelRow.firmwareVersion = line["FirmwareVersion"]
                        self.excelRow.hardwareVersion = line["HardwareVersion"]
                        self.excelRow.battery = line["Battery"]

                        # fetch sensor values Plateaux
                        startPlateauxIndex = 3
                        for x in sensorData:
                            if x < 0 :
                                startPlateauxIndex += 1
                            else:
                                self.excelRow.sensorData[startPlateauxIndex].append(x)

                        # save data to file
                        self.dumpDataToFile()

                        #complete at least one session
                        if self.isArduinoDevice == False:
                            self.isArduinoDevice = True
                            cprint("Save port: %s" % self.portName, "green")
                            self.arduinoPortManager.addValidPortName(self.portName)
                            self.arduinoPortManager.saveValidPortName()
                else : 
                    # new session with testStageId=4
                    # cprint("French format", "yellow")
                    if testStageId == 4:
                        #complete at least one session
                        if self.isArduinoDevice == False:
                            self.isArduinoDevice = True
                            cprint("Save port: %s" % self.portName, "green")
                            self.arduinoPortManager.addValidPortName(self.portName)
                            self.arduinoPortManager.saveValidPortName()

                        # save last session if any
                        if self.excelRow.macAddress != "":
                            self.dumpDataToFile()

                        # update data for new state
                        self.excelRow.macAddress = macAddress
                        self.excelRow.sensorId = sensorId
                        self.excelRow.sensorData[testStageId - 1].extend(sensorData)
                    elif self.excelRow.macAddress != macAddress and self.excelRow.macAddress != "":
                        self.dumpDataToFile()
                    elif self.excelRow.sensorId != sensorId and self.excelRow.sensorId != 0:
                        self.dumpDataToFile()
                    elif testStageId == 8: # complete the test
                        self.excelRow.firmwareVersion = line["FirmwareVersion"]
                        self.excelRow.hardwareVersion = line["HardwareVersion"]
                        self.excelRow.battery = line["Battery"]
                        self.dumpDataToFile()

                        #complete at least one session
                        if self.isArduinoDevice == False:
                            self.isArduinoDevice = True
                            cprint("Save port: %s" % self.portName, "green")
                            self.arduinoPortManager.addValidPortName(self.portName)
                            self.arduinoPortManager.saveValidPortName()
                    else:
                        self.excelRow.macAddress = macAddress
                        self.excelRow.sensorId = sensorId
                        self.excelRow.sensorData[testStageId - 1].extend(sensorData)

            except AssertionError as error:
                print("ERROR: %s" % error)
        except:
            pass

    # def preparePortName(self):
    #     """Opening of the serial port"""
    #     portName = self.readPortName()

    #     # ensure the serial port name is correct
    #     while True:
    #         try:
    #             self.arduino = serial.Serial(portName, 115200, timeout=1)
    #             self.packName = portName
    #             self.savePortName(portName)
    #             print("Receive input from port %s" % portName)
    #             break
    #         except:
    #             print('ERROR: The serial port name is not correct, please check the serial port name and enter again: ')
    #             """Enter serial port name"""
    #             portName = input("Enter serial port name: ")

    def setPort(self, portName):
        # ensure the serial port name is correct
        try:
            self.arduino = serial.Serial(portName, 115200, timeout=1)
            self.isValidPort = True
            self.portName = portName
            cprint("Port: %s --> OK" % portName, "green")
        except:
            cprint('ERROR: serial port: %s not open' % portName, "red")

    def run(self):
        if self.isValidPort == False:
            return

        # open the log data file (*.csv), if the file doesn't exist, create one with header (column name)
        self.openFileAndWriteHeader()
        jsonLine = ""
        """Receiving data and process line by line"""
        while True:
            try:
                rawline = self.arduino.readline()
                replacedline = rawline.replace(b'\r,',b',')
                decodedLine = replacedline.decode('utf-8',"replace").strip()
                print("Slot %d, Received: %s" % (self.slot, decodedLine))
                if len(decodedLine) > 2 :
                    # reset number of package fail
                    self.packageFailCount = 0

                    # receive a package from arduio
                    if decodedLine.startswith("{") :
                      if decodedLine.endswith("}") :
                        # full json text detected
                        jsonLine = ""
                        # cprint("case 1: full json line detected: " + decodedLine, "yellow")
                        self.processLine(decodedLine)
                      else :
                        # cprint("case 2: first part of json line detected: " + decodedLine, "yellow")
                        jsonLine += decodedLine
                    elif decodedLine.endswith("}") and jsonLine != "" :
                      decodedLine = jsonLine + decodedLine
                      jsonLine = ""
                      # cprint("case 3: last part of json line detected: " + decodedLine, "green")
                      self.processLine(decodedLine)
                    else :
                      # cprint("case 4: unexpected text detected: " + decodedLine, "green")
                      jsonLine = ""
                      self.processLine(decodedLine)
                elif self.isArduinoDevice == False and Const.ALLOW_STOP_IDLE_PORT:
                    self.packageFailCount += 1
                    if self.packageFailCount > Const.MAX_PACKAGE_FAIL_ALLOWED :
                        cprint("Stop listen port %s" % self.portName, "yellow")
                        break
            except:
                pass
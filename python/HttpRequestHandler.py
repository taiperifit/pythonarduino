# importing the requests library 
import time
from threading import Thread
import concurrent.futures #for threading
import requests # for http request
from termcolor import colored, cprint # print something with color
import Const

class HttpRequestHandler(Thread):
    #data queue must be json string
    dataQueue = []

    def sendHttpRequest(self, jsonBody) :
        # example of body
        # jsonBody = '{"MacAddress": "T1:T2:T3:T4:T5:T6fd","FirmwareVersion": "Perif","HardwareVersion": "4_2_2","Battery": 100,"Min": 0,"Max": 0,"SensorId": 7,"TestStageId": 8,"SensorData": [1728,1707,1732,1731,1726,1719,1732,1731,1725,1707,1732,1731,1724,1705,1731,1731,-1,1668,1730,1722,1706,1729,1729,1723,1705,1730,1729,1723,1706,1730,1729,1723,1706]}'
        # cprint ('trying to send request: %s' % jsonBody)
        try:
            headers = {'Content-Length' : str(len(jsonBody)), 'Content-Type' : 'application/json'}
            r = requests.post(url = Const.API_END_POINT, data=jsonBody, headers=headers, timeout=Const.REQUEST_TIMEOUT)

            #send succeeded 
            cprint('HttpRequest -> OK, response message: %s' % (r.text), 'white')
            return True
        except requests.Timeout:
            # back off and retry
            cprint('HttpRequest -> ERROR: Connection timeout, retry ...', 'yellow', 'on_red')
            return False

        except requests.ConnectionError:
            #something wrong with server, stop connection
            cprint('HttpRequest -> ERROR: No internet connection, please check the internet connection or contact your admin', 'yellow', 'on_red')
            return False

    def addData(self, data):
        self.dataQueue.append(data)
    
    def run(self):
        # loop forever to trigger sending request if any
        while True:
            # print("running.....%d" % checkNum)
            while len(self.dataQueue) > 0 :
                data = self.dataQueue[0]
                result = self.sendHttpRequest(data)
                if result :
                    #remove data when sending succeeded
                    del self.dataQueue[0]

                # sleep 1 second for next request
                time.sleep(1)
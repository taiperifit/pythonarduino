from json import JSONEncoder
import json
import inspect

class BackendDataFormatEncoder(JSONEncoder):
    def default(self, obj):
        if hasattr(obj, "to_json"):
            return self.default(obj.to_json())
        elif hasattr(obj, "__dict__"):
            d = dict(
                (key, value)
                for key, value in inspect.getmembers(obj)
                if not key.startswith("__")
                and not inspect.isabstract(value)
                and not inspect.isbuiltin(value)
                and not inspect.isfunction(value)
                and not inspect.isgenerator(value)
                and not inspect.isgeneratorfunction(value)
                and not inspect.ismethod(value)
                and not inspect.ismethoddescriptor(value)
                and not inspect.isroutine(value)
            )
            return self.default(d)
        return obj

class BackendDataFormat:
    def __init__(self):
        self.MacAddress = ""
        self.FirmwareVersion = ""
        self.HardwareVersion = ""
        self.Battery = 0
        self.Min = 0
        self.Max = 0
        self.SensorId = 7
        self.TestStageId = 8
        self.SensorData = []

    def reset(self):
        self.MacAddress = ""
        self.FirmwareVersion = ""
        self.HardwareVersion = ""
        self.Battery = 0
        self.Min = 0
        self.Max = 0
        self.SensorId = 7
        self.TestStageId = 8
        self.SensorData = []

    def toJson(self):
        return json.dumps(self, cls=BackendDataFormatEncoder, sort_keys=False)
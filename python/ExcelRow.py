import datetime
import Const
from termcolor import colored, cprint # print something with color
from BackendDataFormat import BackendDataFormat

class ExcelRow:
    def __init__(self):
        self.macAddress = ""
        self.sessionDate = datetime.datetime.now().strftime("%d/%m/%Y")
        self.sessionTime = datetime.datetime.now().strftime("%H:%M")
        self.sensorId = 0
        self.testStage = ""
        self.sensorData = [[],[],[],[],[],[],[]]
        self.firmwareVersion = ""
        self.hardwareVersion = ""
        self.battery = ""
        self.minSensorData = 0
        self.maxSensorData = 0
        self.status = Const.STATUS_COMPLETE
        self.peakData = BackendDataFormat()
        self.is2TestStageId = False # 2 test stage: chinese format, 8 test stage: French format

    def setStageFormat(self, is2TestStageId):
        self.is2TestStageId = is2TestStageId

    def dumpDataToFile(self, file):
        writeFormat = "%s,"
        testStatus = self.getStatus()
        if testStatus == Const.STATUS_COMPLETE:
            cprint('[%s] Session status: %s' % (self.macAddress, testStatus), 'white')
        else:
            cprint('[%s] Session status: %s' % (self.macAddress, testStatus), 'red')
        # Mac address
        file.write(self.macAddress + ',')

        # Session date
        file.write(writeFormat % self.sessionDate)

        # Session time
        file.write(writeFormat % self.sessionTime)

        # Sensor name: superficial/deep
        if self.sensorId == 1:
            file.write(Const.SUPERFICIAL_SENSOR + ',')
        else:
            file.write(Const.DEEP_SENSOR + ',')

        # This order base on data receive from Arduino
        stateOrder = [4,1,5,2,6,3,7]
        for stageId in stateOrder:
            index = stageId - 1
            # Stage Name
            file.write(writeFormat % Const.LIST_TEST_STATE_ID_NAME[index])

            # Sensor data
            sensorDataCount = len(self.sensorData[index])
            if sensorDataCount < Const.MAX_SENSOR_VALUE_COUNT:
                for x in range(Const.MAX_SENSOR_VALUE_COUNT - sensorDataCount):
                    self.sensorData[index].append(-1)
            elif sensorDataCount > Const.MAX_SENSOR_VALUE_COUNT:
                cprint("WARNING: the length of sensor data at stage %d is too long max allowed: %d values, data sent: %d value" % (stageId, Const.MAX_SENSOR_VALUE_COUNT, sensorDataCount), 'yellow' )
            for x in range(Const.MAX_SENSOR_VALUE_COUNT):
                sensorValue = self.sensorData[index][x]
                if sensorValue < 0:
                    sensorValue = ""
                file.write(writeFormat % sensorValue)

        # Firmware version
        file.write(writeFormat % self.firmwareVersion)

        # Hardware version
        file.write(writeFormat % self.hardwareVersion )

        #Battery
        file.write(writeFormat % self.battery)

        #$tatus
        file.write("%s\n" % testStatus)

    def getStatus(self):
        isCompleted = True
        if self.is2TestStageId:
            sensorValueIndex = [0,1,3,4]
            for x in sensorValueIndex:
                if len(self.sensorData[x]) == 0:
                    isCompleted = False
                    break

            if isCompleted:
                return Const.STATUS_COMPLETE
        else:
            for sensor in self.sensorData:
                if len(sensor) == 0:
                    isCompleted = False
                    break

            if isCompleted:
                return Const.STATUS_COMPLETE

        return Const.STATUS_INCOMPLETE

    def reset(self):
        self.macAddress = ""
        self.sessionDate = datetime.datetime.now().strftime("%d/%m/%Y")
        self.sessionTime = datetime.datetime.now().strftime("%H:%M")
        self.sensorId = 0
        self.testStage = ""
        self.sensorData = [[],[],[],[],[],[],[]]
        self.firmwareVersion = ""
        self.hardwareVersion = ""
        self.battery = ""
        self.status = Const.STATUS_COMPLETE
        self.peakData.reset()
        self.is2TestStageId = False

    def getPeakBackendData(self):
        self.peakData.reset()
        self.peakData.MacAddress = self.macAddress
        self.peakData.FirmwareVersion = self.firmwareVersion
        self.peakData.HardwareVersion = self.hardwareVersion
        self.peakData.Battery = self.battery
        self.peakData.Min = 0
        self.peakData.Max = 0
        self.peakData.SensorId = self.sensorId
        self.peakData.TestStageId = 7
        for x in range(2):
            if x > 0:
                self.peakData.SensorData.append(-1)
            self.peakData.SensorData.extend(self.sensorData[x])
        jsonString = self.peakData.toJson()
        self.peakData.reset()
        return jsonString

    def getPlateauxBackendData(self):
        self.peakData.reset()   
        self.peakData.MacAddress = self.macAddress
        self.peakData.FirmwareVersion = self.firmwareVersion
        self.peakData.HardwareVersion = self.hardwareVersion
        self.peakData.Battery = self.battery
        self.peakData.Min = 0
        self.peakData.Max = 0
        self.peakData.SensorId = self.sensorId
        self.peakData.TestStageId = 8
        for x in range(3, 5):
            if x > 3:
                self.peakData.SensorData.append(-1)
            self.peakData.SensorData.extend(self.sensorData[x])
        jsonString = self.peakData.toJson()
        self.peakData.reset()
        return jsonString
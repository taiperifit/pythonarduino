CONFIG_FOLDER = "Config"
EXPORT_FOLDER = "Exported"
MAX_PACKAGE_FAIL_ALLOWED = 10
ALLOW_STOP_IDLE_PORT = False

MAX_SENSOR_VALUE_COUNT = 30
STATUS_COMPLETE = "Complete"
STATUS_INCOMPLETE = "Incomplete"
SUPERFICIAL_SENSOR = "Superficial"
DEEP_SENSOR = "Deep"
LIST_TEST_STATE_ID_NAME = [ "Peak 1",       #Stage Id: 1
                            "Peak 2",       #Stage Id: 2
                            "Peak 3",       #Stage Id: 3
                            "Plateau 0",    #Stage Id: 4
                            "Plateau 1",    #Stage Id: 5
                            "Plateau 2",    #Stage Id: 6
                            "Plateau 3"]    #Stage Id: 7

CHINESE_FORMAT_START_STATE = 7
CHINESE_FORMAT_END_STATE = 8

# END POINT DEFINE
REQUEST_TIMEOUT = 10 #in second
SERVER_IP = "34.242.160.143"
SERVER_PORT = "8080"
API_END_POINT = ("http://%s:%s/perifitTest" % (SERVER_IP, SERVER_PORT))
# -*- coding: utf-8 -*-
from termcolor import colored, cprint # print something with color
from JigLog import JigLog
from HttpRequestHandler import HttpRequestHandler
from ArduinoPortManager import ArduinoPortManager

# handle http request (must run on thread)
httpRequestHandler = HttpRequestHandler()

# Arduino port manager get all valid port
arduinoPortManager = ArduinoPortManager()

# Set up for JigLog
JigLog.httpRequestHandler = httpRequestHandler
JigLog.arduinoPortManager = arduinoPortManager

validPortName = arduinoPortManager.getAllComPortsWithFillter()
potentialPortName = arduinoPortManager.getAllPotentialPortName()

# print ("====== ARDUINO DEVICE DETECTED ====")
# for x in validPortName:
#     print("Port %s" % x)
# print("=====================\n")

# print ("====== POTENTIAL DEVICE DETECTED ====")
# for x in potentialPortName:
#     print("Port %s" % x)
# print("=====================\n")

# list of jig what plugged
jigLogList = []
jigLogCount = len(validPortName)
jigLogPotentialCount = len(potentialPortName)

#make sure at least one arduino plugged
while (jigLogCount + jigLogPotentialCount) == 0 :
    cprint("ERROR: no adrduino device found")
    confirm = input("Please plug your adrduino devices then press [Y]")
    if confirm.lower() == "y" :
        validPortName = arduinoPortManager.getAllComPorts()
        jigLogCount = len(validPortName)
        
        potentialPortName = arduinoPortManager.getAllPotentialPortName()
        jigLogPotentialCount = len(potentialPortName)
    
# create list of valid jig log
for x in range(jigLogCount):
    jigLogList.append(JigLog(True))

# create list of jig
slot = 0
for x in range(jigLogCount):
    jigLog = jigLogList[x]
    jigLog.setSlot(slot)
    jigLog.setPort(validPortName[x])
    slot = slot + 1
    
# create list of potential jig log
for x in range(jigLogPotentialCount):
    jigLogList.append(JigLog(False))

for x in range(jigLogPotentialCount):
    jigLog = jigLogList[x + jigLogCount]
    jigLog.setSlot(slot)
    jigLog.setPort(potentialPortName[x])
    slot = slot + 1

# trigger sending data to backend
httpRequestHandler.start()

# run all jigs
for jiglog in jigLogList:
    jiglog.start()

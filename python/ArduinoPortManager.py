import serial.tools.list_ports
import serial
import os
import Const

class ArduinoPortManager:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # declare propertices here
        self.ignoreDeviceList = []
        self.validPortName = []

        # load arduino port what we detected before
        self.loadValidPortName()

    def getValidPortName(self):
        return self.validPortName

    def loadValidPortName(self):
         # create Config folder if it doesn't exist
        configFolder = Const.CONFIG_FOLDER
        configFileName = "%s/valid_ports.config" % (configFolder)
        if os.path.isdir(configFolder) == False :
            return
            
        if os.path.isfile(configFileName) == False:
            return
        
        filehandle = open(configFileName, 'r')
        while True:
            # read a single line
            line = filehandle.readline()
            if not line:
                break
            line = line.replace("\n", "")
            if len(line) == 0:
                continue
            self.validPortName.append(line)

        # close the pointer to that file
        filehandle.close()

    def saveValidPortName(self):
        configFolder = Const.CONFIG_FOLDER
        configFileName = "%s/valid_ports.config" % (configFolder)

        if os.path.isdir(configFolder) == False :
            os.mkdir(configFolder)

        file=open(configFileName, mode='w')
        for portName in self.validPortName :
            file.write("%s\n" % portName)
        file.close()

    def addValidPortName(self, portName):
        if portName not in self.validPortName :
            self.validPortName.append(portName)

    def getAllComPortsWithFillter(self) :
        filterName = 'arduino'
        comports = serial.tools.list_ports.comports()
        for port in comports:
            device = str(port.device)
            product = str(port.product)
            manufacturer = str(port.manufacturer)
            # product/manufacturer contain 'arduino'
            if filterName in product.lower() or filterName in manufacturer.lower() :
                self.addValidPortName(device)

        # save all valid port for next run
        self.saveValidPortName()

        return self.validPortName

    def getAllPotentialPortName(self) :
        comports = serial.tools.list_ports.comports()
        potentialPort = []
        for port in comports:
            device = str(port.device)
            if device not in self.validPortName :
                potentialPort.append(device)

        return potentialPort